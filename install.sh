#!/bin/bash

#######################################
## Installation for TYPO3
#######################################

version="v.0.0.7";

install() {
    startCounter=$(date +%s)
    clear

    if [ ! -f "install.ini" ]; then
       errorMsg ":: Error [g3978p78jzk312a0el4v1nd4ut6tb3z8] in configuration\n" \
        ":: File <install.ini> does not exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    #include config to create a new project
    . install.ini
    
    if [ ! -f "install.local.ini" ]; then
       errorMsg ":: Error [g3978p78jzk312a0el4v1nd4ut6tb3z8] in configuration\n" \
        ":: File <install.local.ini> does not exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    #include config to create a new project
    . install.local.ini

    boilerplatename=$t_boilerplate
    repoThm=$repothm

    infoMsg "::\n" \
    ":: Version: $version\n" \
    "::\n" \
    ":: Variables:\n" \
    ":: t_boilerplate: $boilerplatename\n" \
    ":: repothm: $repothm\n" \
    "::\n"

    read -rsp $'\n\nPress any key to continue...\n' -n1 key

    #check placeholder variables
    if [ "$repoThm" == "XXX_YY_xxx_thm" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \$repothm must not be '$repoThm'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    if [ "$repoThm" == "teufels_thm_custom" ]; then
       errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \$repothm must not be '$repoThm'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    if [ "$boilerplatename" == "t_boilerplate_YY-mm-dd_HH-MM-SS" ]; then
       errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \boilerplatename must not be '$boilerplatename'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    #check if docker-compose.yml exists
    if [ ! -f "../${boilerplatename}/docker-compose.yml" ]; then
       errorMsg ":: Error [g3978p78jzk312a0el4v1nd4ut6tb3z8] in configuration\n" \
        ":: File <../${boilerplatename}/docker-compose.yml> does not exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    #check if docker-sync.yml doesn't exists
    if [ -f "../${boilerplatename}/docker-sync.yml" ]; then
    errorMsg ":: Error [fsf9qxy3i3pi4k4usi18umx21r9q837v] in configuration\n" \
        ":: File <../${boilerplatename}/docker-sync.yml> does exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    logMsg "install TYPO3"


    ###### create database .ini files ######
    logMsg "create database .ini files"
    cp -v "src/expample.ini" "src/development.ini"
    cp -v "src/expample.ini" "src/staging.ini"
    cp -v "src/expample.ini" "src/production1.ini"
    cp -v "src/expample.ini" "src/production.ini"
    cp -v "src/expample.ini" "src/hotfix.ini"


    ###### start docker ######
    cd ../${boilerplatename}

    #stop all running docker containers
    logMsg "stopping all running docker containers"
    docker stop $(docker ps -a -q)
    sleep 2

    #start docker
    logMsg "start docker-compose up -d"
    docker-compose up -d
    sleep 12


    ###### copy composer.json with forked thm ######
    cd ../t_installer/src
    logMsg "set thm variable to composer.json"
    sed -ie "s/XXX_YY_xxx_thm/$repoThm/" composer.json
    infoMsg "::\n" \
    ":: diff composer.jsone composer.json\n" \
    "::"
    nbsp
    infoMsg "-+"
    diff -by -W245 --suppress-common-lines ./composer.jsone ./composer.json
    infoMsg "-+"
    nbsp

    rm -rf composer.jsone

    logMsg "copy composer.json to ${boilerplatename}"
    cp -n "composer.json" "../../${boilerplatename}/app/composer.json"
    sleep 1


    ###### composer install ######
    cd ../../${boilerplatename}/app
	composer install
    sleep 1

    logMsg "create FIRST_INSTALL file"
	cd web
	touch FIRST_INSTALL
	sleep 1


    ###### copy dummy database and install_extensions.sh to t_boilerplate directories ######
    logMsg "copy dummy database and install_extensions.sh to ${boilerplatename}"
    cd ../../..

    if [ ! -d "$boilerplatename/backup" ]; then
       mkdir ${boilerplatename}/backup
    fi

    cp -v "t_installer/src/TYPO3dummy.sql" "${boilerplatename}/backup/TYPO3dummy.sql"
    cp -v "t_installer/src/install_extensions.sh" "${boilerplatename}/app/install_extensions.sh"
    sleep 1


    ###### browser configuration ######
    open "http://development.localhost/typo3/sysext/install/Start/Install.php"
    logMsg "open http://development.localhost/typo3/sysext/install/Start/Install.php"
    infoMsg "::\n" \
    "::\n" \
    ":: Development database settings:\n" \
    ":: Username: dev\n" \
    ":: Password: dev\n" \
    ":: Type: TCP / IP based conenction\n" \
    ":: Host: mysql\n" \
    ":: Port: 3306\n" \
    "::"
	sleep 1
	read -rsp $'\n\nPress any key to continue...\n' -n1 key


    ###### import dummy database ######
    logMsg "start dummy database import"
    cd ${boilerplatename}
    make -f Makefile.teufels mysql-import-TYPO3dummy
    sleep 1


    ###### install extensions ######
    logMsg "install default extensions"
	make -f Makefile.teufels install-extensions
	sleep 1


	###### copy cooluriconf ######
    logMsg "copy CoolUriConf.xml to typo3conf folder"
    cp -v app/web/typo3conf/ext/cooluri/Resources/CoolUriConf.xml_default app/web/typo3conf/CoolUriConf.xml
    cd ..


    ###### delete symlink ######
    logMsg "delete index.php symlink"
    rm -rf ${boilerplatename}/app/web/index.php
    sleep 1


    ###### copy src files to t_boilerplate directories ######
    logMsg "copy files to ${boilerplatename}"

    cp -v "t_installer/src/_.htaccess" "${boilerplatename}/app/web/.htaccess"
    cp -v "t_installer/src/local.ini" "${boilerplatename}/app/web/typo3conf/local.ini"
    cp -v "t_installer/src/_AdditionalConfiguration.php" "${boilerplatename}/app/web/typo3conf/AdditionalConfiguration.php"
    cp -v "t_installer/src/_index.php" "${boilerplatename}/app/web/index.php"
    cd t_installer

    logMsg "script finished"
    endCounter=$(date +%s)
    totalTime=$(echo "$endCounter - $startCounter" | bc)
    echo " total time:" $(convertsecs $totalTime)
}

convertsecs() {
    h=`expr $1 / 3600`
    m=`expr $1  % 3600 / 60`
    s=`expr $1 % 60`
    printf "%02d:%02d:%02d\n" $h $m $s
}

errorMsg() {
    echo -e "" \
    $(tput setaf 1)::$(tput sgr 0) "\n"\
    $(tput setaf 1)$*$(tput sgr 0) "\n"\
    $(tput setaf 1)::$(tput sgr 0)
}

logMsg() {
    echo -e "" \
    "$*"
}

infoMsg() {
    echo -e "" \
    "$*"
}

nbsp() {
    echo ""
}

install