#!/bin/bash

#######################################
## install extensions
#######################################

initInstallExtensions() {
    logMsg "start install extensions"

    logMsg "install flux"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install flux
    logMsg "install fluidcontent"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install fluidcontent
    logMsg "install vhs"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install vhs

    logMsg "install teufels_cfg_page"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cfg_page
    logMsg "install teufels_cfg_typoscript"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cfg_typoscript
    logMsg "install teufels_cfg_user"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cfg_user
    logMsg "install teufels_cpt_brand"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_brand

    logMsg "install teufels_cpt_cnt_bs_btn"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_cnt_bs_btn
    logMsg "install teufels_cpt_cnt_bs_carousel"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_cnt_bs_carousel
    logMsg "install teufels_cpt_cnt_bs_collapsible"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_cnt_bs_collapsible
    logMsg "install teufels_cpt_cnt_bs_coltwo"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_cnt_bs_coltwo
    logMsg "install teufels_cpt_cnt_bs_falimg"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_cnt_bs_falimg
    logMsg "install teufels_cpt_cnt_bs_tab"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_cnt_bs_tab
    logMsg "install teufels_cpt_cnt_falimg"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_cnt_falimg
    logMsg "install teufels_cpt_cnt_img"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_cnt_img

    logMsg "install teufels_cpt_dynamiccontent"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_dynamiccontent
    logMsg "install teufels_cpt_nav_anchor"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_nav_anchor
    logMsg "install teufels_cpt_nav_breadcrumb"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_nav_breadcrumb
    logMsg "install teufels_cpt_nav_mega"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_nav_mega
    logMsg "install teufels_cpt_nav_mobile"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_nav_mobile
    logMsg "install teufels_cpt_nav_simple"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_nav_simple
    logMsg "install teufels_cpt_nav_simple_dd"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_nav_simple_dd
    logMsg "install teufels_cpt_nav_special"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_nav_special
    logMsg "install teufels_cpt_nav_sub"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_cpt_nav_sub

    logMsg "install teufels_ovr_fluidstyledcontent"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_ovr_fluidstyledcontent
    logMsg "install metaseo"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install metaseo
    logMsg "install cooluri"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install cooluri
    logMsg "install powermail"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install powermail
    logMsg "install news"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install news
    logMsg "install teufels_ovr_metaseo"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_ovr_metaseo
    logMsg "install sourceopt"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install sourceopt
    logMsg "install teufels_ovr_sourceopt"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_ovr_sourceopt

    logMsg "install teufels_thm_less"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_less
    logMsg "install teufels_thm_backendlayout"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_backendlayout
    logMsg "install teufels_thm_blazy"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_blazy
    logMsg "install teufels_thm_bs"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_bs
    logMsg "install teufels_thm_bs_toolkit"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_bs_toolkit
    logMsg "install teufels_thm_custom"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_custom
    logMsg "install teufels_thm_ie_dinosaurs"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_ie_dinosaurs
    logMsg "install teufels_thm_jq"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_jq
    logMsg "install teufels_thm_jq_anima"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_jq_anima
    logMsg "install teufels_thm_jq_echo"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_jq_echo
    logMsg "install teufels_thm_jq_focuspoint"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_jq_focuspoint
    logMsg "install teufels_thm_jq_hammer"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_jq_hammer
    logMsg "install teufels_thm_jq_transit"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_jq_transit
    logMsg "install teufels_thm_modernizr"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_modernizr
    logMsg "install teufels_thm_pace"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_pace
    logMsg "install teufels_thm_webfontloader"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_thm_webfontloader

    logMsg "install teufels_vh"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_vh
    logMsg "install teufels_h"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_h
    logMsg "install teufels_kueche"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install teufels_kueche

    logMsg "finish install extensions"
}

logMsg() {
    echo -e "" \
    "$*"
}

initInstallExtensions